import { shallowMount } from '@vue/test-utils'
import Calc from '../../src/components/Calc.vue'

const factory = (values = {}) => {
  return shallowMount(Calc, {
    data () {
      return {
        ...values
      }
    }
  })
}

describe('Calc', () => {
  it('Verifica divisão correta', () => {
    const wrapper = factory({
      A: 10,
      B: 2
    })
    wrapper.find('button').trigger('click')
    expect(wrapper.vm.result).toBe(5)
  })
})
